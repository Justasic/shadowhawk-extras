import asyncio
import ziproto
import time
import math
import uuid
import json
import rrdtool
import traceback
import threading
import ormar
import enum
import typing
from datetime import datetime, timedelta
import uuid as uniqueid
from pathlib import Path
from io import BytesIO
from numpy import mean
from asyncevents import emit, wait, on_event
from pyrogram import Client, filters, ContinuePropagation
from pyrogram.types import Message
from shadowhawk import config, slave, loop, app_user_ids, apps
from shadowhawk.utils import build_name_flags, get_duration, get_entity, get_chat_link, self_destruct, name_escape
from shadowhawk.utils.Logging import log_errors, public_log_errors
from shadowhawk.database import BaseModel
from . import sudoers
from shadowhawk.plugins.log_user_joins import sexy_user_name
from pyrogram.errors.exceptions.flood_420 import FloodWait
from pyrogram.errors.exceptions.bad_request_400 import MessageTooLong
from shadowhawk import ObjectProxy
# from shadowhawk.plugins.sibyl import sibyl_client

DEBUG = False

if DEBUG:
	spamreportchats = filters.chat([-1001450488581])
else:
	spamreportchats = filters.chat([-1001573521324])

active_events = {}
exp_lock = asyncio.Lock()

# Iteration every 5 seconds on a timescale of 30 seconds
TIMESCALE = 5.0
timescale_magic = math.exp(-TIMESCALE / 30)
dblock = asyncio.Lock()

# Our RRD database paths, these should be kept in persistent storage.
RRD_DB_DAY = "sessions/massadd.rrd"
RRD_DB_WEEK = "sessions/massadd_week.rrd"
RRD_DB_MONTH = "sessions/massadd_month.rrd"

# Message templates.
add_template = """
<b>Mass Add Event</b> [#MASSADD]
- Chat: {chatname}
- Event ID: <code>{uuid}</code>
- Message: <a href="{msglink}">link</a>
- Rate: <code>{rate:.3f}</code>
- APS: <code>{avgaps:.3f}</code>
- Date: <code>{date}</code>
- Duration: <code>{delta}</code>
<b>Participants:</b>
"""
add_conclude_template = """<b>Event Conclusion</b>
Total Added: {totaladd}
Total Participants: {totalparticipants}
"""

class MAUser(ormar.Model):
	class Meta(BaseModel):
		pass

	id: int = ormar.BigInteger(primary_key=True)
	first_name: typing.Optional[str] = ormar.String(max_length=64, nullable=True)
	last_name: typing.Optional[str] = ormar.String(max_length=64, nullable=True)
	username: typing.Optional[str] = ormar.String(max_length=32, nullable=True)
	record_creation: datetime = ormar.DateTime(default=datetime.now())
	record_updated: datetime = ormar.DateTime(default=datetime.now())

	# Account that saw this user will be stored in metadata.
	metadata: typing.Optional[bytes] = ormar.LargeBinary(max_length=1073742000, nullable=True)

class MAChatroom(ormar.Model):
	class Meta(BaseModel):
		pass

	id: int = ormar.BigInteger(primary_key=True)
	username: typing.Optional[str] = ormar.String(max_length=32, nullable=True)
	title: str = ormar.String(max_length=256)
	record_creation: datetime = ormar.DateTime(default=datetime.now())
	record_updated: datetime = ormar.DateTime(default=datetime.now())
	metadata: typing.Optional[bytes] = ormar.LargeBinary(max_length=1073742000, nullable=True)

class MAEvent(ormar.Model):
	class Meta(BaseModel):
		pass

	uuid: uniqueid.UUID = ormar.UUID(primary_key=True)
	chat: MAChatroom = ormar.ForeignKey(MAChatroom)
	aps: float = ormar.Float()
	begin_time: datetime = ormar.DateTime(default=datetime.now())
	end_time: datetime = ormar.DateTime(default=datetime.now())
	# participants: typing.List['MAAddRecord'] = ormar.ManyToMany('MAAddRecord', virtual=True, related_name="eventparticipants")

	# Log message location will be kept in metadata blob.
	metadata: typing.Optional[bytes] = ormar.LargeBinary(max_length=1073742000, nullable=True)

class MAAddRecord(ormar.Model):
	class Meta(BaseModel):
		pass

	int: id = ormar.BigInteger(primary_key=True, autoincrement=True)
	creation: typing.Optional[datetime] = ormar.DateTime(default=datetime.now(), nullable=True)
	adder: MAUser = ormar.ForeignKey(MAUser, related_name="maadders")
	added: MAUser = ormar.ForeignKey(MAUser, related_name="maadded")
	event: MAEvent = ormar.ForeignKey(MAEvent, related_name="maevents")

class UserBlob(enum.IntEnum):
	USER_ID = 0
	USER_FIRSTNAME = 1
	USER_LASTNAME = 2
	USER_CREATION = 3
	USER_UPDATE = 4
	USER_USERNAME = 5

class ChatBlob(enum.IntEnum):
	CHAT_ID = 0
	CHAT_ID_SHORT = 1
	CHAT_USERNAME = 2
	CHAT_TITLE = 3
	CHAT_CREATION = 4
	CHAT_UPDATED = 5
	CHAT_ADDEDCNT = 6

class AdderBlob(enum.IntEnum):
	ADDER_ID = 0
	ADDER_FIRSTNAME = 1
	ADDER_LASTNAME = 2
	ADDER_USERNAME = 3
	ADDER_CREATION = 4
	ADDER_UPDATED = 5
	ADDER_ADDED = 6
	ADDER_EVENTS = 7
	ADDER_SEENACT = 8

class EventBlob(enum.IntEnum):
	EVENT_UUID = 0
	EVENT_CHATID = 1
	EVENT_APS = 2
	EVENT_BEGIN = 3
	EVENT_END = 4
	EVENT_PARTICIPANTS = 5
	EVENT_LOGMSG = 6

# Invent the timescale for detecting spikes in spam adds
async def exp_normalize():
	global TIMESCALE
	# Handle floodwaits differently by continuing without editing the msg
	edit_floodwait = 0
	msg_floodwait = 0
	while True:
		try:
			async with exp_lock:

				# Update the RRD graphs
				event_cnt = len(active_events.keys())
				rrdtool.update(RRD_DB_DAY, f"N:{event_cnt}")
				# rrdtool.update(RRD_DB_WEEK, f"N:{event_cnt}")
				# rrdtool.update(RRD_DB_MONTH, f"N:{event_cnt}")

				# removeme = []
				# because we need to delete stuff while iterating, use keys instead.
				for key in list(active_events.keys()):
					tracked_data = active_events[key]
					tracked_data['load'] *= timescale_magic
					tracked_data['load'] += tracked_data['add_cnt'] * (1 - timescale_magic)

					# Figure out the delta.
					delta = datetime.now() - tracked_data['date']

					# Calculate the current aps
					# adds / sleep time -- sorta accurate as long as we don't floodwait.
					aps = tracked_data['add_cnt'] / 5
					tracked_data['aps'].append(aps)
					# Reset the add count
					tracked_data['add_cnt'] = 0

					# sub our floodwaits
					if edit_floodwait > 0:
						edit_floodwait -= int(TIMESCALE)
					if msg_floodwait > 0:
						msg_floodwait -= int(TIMESCALE)
					# Clamp our floodwaits (we can't wait negative seconds)
					if edit_floodwait < 0:
						edit_floodwait = 0
					if msg_floodwait < 0:
						msg_floodwait = 0

					# Announce a mass-add update to everyone
					await emit("OnMassAddUpdate", block=False, event_data=tracked_data)

					# We're floodwaited, don't do anything until it's over with.
					if edit_floodwait > 0 or msg_floodwait > 0:
						continue # Don't conclude yet, we're floodwaited so let's wait.

					# get a random client so things can be resolved.
					client = app_user_ids[list(app_user_ids.keys())[0]]

					# Consider the spam-add event concluded.
					if tracked_data['load'] <= 0.05 and tracked_data['msg']:
						participanttext = ""
						if tracked_data['toobig']:
							participanttext = "<i>Too many participants, will generate file upon event conclusion</i>\n"
							participanttext += f"- {len(tracked_data['participants'])} partipants\n"
						else:
							for usrid, usr in tracked_data['participants'].items():
								participanttext += f"\u200E-- {sexy_user_name(usr[0])} added: {usr[1]}\n"
						# begin our template
						link = f"https://t.me/c/{str(tracked_data['chatobj'].id)}/{str(tracked_data['message_id'])}"
						text = add_template.format(**{
							'chatname': await build_name_flags(client, tracked_data['chatobj']),
							'msglink': link,
							'uuid': tracked_data['uuid'],
							'rate': tracked_data['load'],
							'avgaps': mean(tracked_data['aps']),
							'date': tracked_data['date'].strftime("%c"),
							'delta': get_duration(delta.total_seconds())
						})
						text += participanttext
						text += add_conclude_template.format(**{
							'totaladd': tracked_data['total_cnt'],
							'totalparticipants': len(tracked_data['participants'].keys())
						})

						# Editing the message can cause floodwaits and if there's too many people
						# we need to do something else. Try and handle it better.
						try:
							await tracked_data['msg'].edit(text, disable_web_page_preview=True)
						except FloodWait as ex:
							print(f"Sleeping for {ex.value + 1}s for edit_message")
							edit_floodwait = ex.value + 1
							continue
						# For when stupid bullshit happens and this dumb ass function can't figure
						# it out at all. Whatever, catch the exception and mark the shit as too big.
						except MessageTooLong:
							tracked_data['toobig'] = True
							# Since we're literally about to send as a file, just do it this way.

						if tracked_data['toobig']:
							link = f"https://t.me/c/{str(tracked_data['chatobj'].id)}/{str(tracked_data['message_id'])}"
							# Send our full report as a file.
							fmsg = add_template.format(**{
								'chatname': await build_name_flags(client, tracked_data['chatobj']),
								'msglink': link,
								'uuid': tracked_data['uuid'],
								'rate': tracked_data['load'],
								'avgaps': mean(tracked_data['aps']),
								'date': tracked_data['date'].strftime("%c"),
								'delta': get_duration(delta.total_seconds())
							})
							for usrid, usr in tracked_data['participants'].items():
								fmsg += f"\u200E-- {sexy_user_name(usr[0])} added: {usr[1]}\n"
							fmsg += add_conclude_template.format(**{
								'totaladd': tracked_data['total_cnt'],
								'totalparticipants': len(tracked_data['participants'].keys())
							})
							f = BytesIO(fmsg.strip().encode('utf-8'))
							f.name = f"massadd_{tracked_data['chatobj'].id}.txt"
							try:
								await tracked_data['msg'].reply_document(f, caption=f"<code>{tracked_data['uuid']}</code>")
							except FloodWait as ex:
								print(f"Sleeping for {ex.value + 1}s for reply_document")
								msg_floodwait = ex.value + 1
								continue

						# Announce a completion event
						tracked_data_proxy = ObjectProxy(tracked_data)
						await emit("OnPreMassAddComplete", block=True, tracked_data_proxy=tracked_data_proxy)
						tracked_data = tracked_data_proxy.get_thing()

						event_array = {
							EventBlob.EVENT_LOGMSG: (tracked_data['msg'].chat.id, tracked_data['msg'].id)
						}

						# Update the record
						event = tracked_data['eventobj']
						event.aps = mean(tracked_data['aps'])
						event.metadata = bytes(ziproto.encode(event_array))
						await event.update()

						# Make sure we plan to remove the event.
						del active_events[key]

						# Announce the completion of the event
						await emit("OnMassAddComplete", block=False, event=event)

					# begin a spam add event if they basically add more than 1 user, update it if an event exists already
					elif tracked_data['load'] >= 0.5:
						participanttext = ""
						if (tracked_data['toobig']):
							participanttext = "<i>Too many participants, will generate file upon event conclusion</i>\n"
							participanttext += f"- {len(tracked_data['participants'])} partipants\n"
						else:
							for usrid, usr in tracked_data['participants'].items():
								participanttext += f"\u200E-- {sexy_user_name(usr[0])} added: {usr[1]}\n"

						link = f"https://t.me/c/{str(tracked_data['chatobj'].id)}/{str(tracked_data['message_id'])}"
						# begin our template
						text = add_template.format(**{
							'chatname': await build_name_flags(client, tracked_data['chatobj']),
							'msglink': link,
							'uuid': tracked_data['uuid'],
							'rate': tracked_data['load'],
							'avgaps': mean(tracked_data['aps']),
							'date': tracked_data['date'].strftime("%c"),
							'delta': get_duration(delta.total_seconds())
						})
						text += participanttext

						try:
							if not tracked_data['msg']:
								tracked_data['msg'] = await slave.send_message(config['config']['massadd']['log_location'], text, disable_web_page_preview=True)
							else:
								await tracked_data['msg'].edit(text, disable_web_page_preview=True)
						except MessageTooLong:
							tracked_data['toobig'] = True
							continue
						except FloodWait as ex:
							print(f"Sleeping for {ex.value + 1}s for send_message")
							msg_floodwait = ex.value + 1
							continue
					# Reap old values
					if tracked_data['load'] <= 0.05 and tracked_data['date'] + timedelta(minutes=2) <= datetime.now():
						# Announce a completion event.
						if key in active_events:
							await emit("OnMassAddCompleteNoRecord", block=False, tracked_data=tracked_data)
							del active_events[key]
		except BaseException:
			traceback.print_exc()
		await asyncio.sleep(TIMESCALE)

# Function to calculate whatever this steps number needs to be
# for the graph size we plan to use.
def calc_steps(days: int, step: int = 5, graphsz: int = 400) -> int:
	day = 24 * 60 * 60  # seconds in a day
	# Get the steps per day
	spd = int(day / step)
	# calculate whatever this number is
	return int((days * spd) / graphsz)

@on_event("OnDatabaseStart")
async def OnStart(_, EventName: str):
	# create our new task
	asyncio.create_task(exp_normalize())
	# asyncio.create_task(process_actions())
	# Start the RRD graphs if they don't already exist.
	# Each graph has a step of 5 seconds but different
	# time periods.
	# Read: https://cuddletech.com/articles/rrd/ar01s02.html
	#####################################
	# In these graphs, we sample every 5 seconds from our loop
	# at the top of this file. RRD will consolidate these samples
	# into steps which will be rendered on the graph in the end.
	##
	# We assume the graph size will be 400x100 pixels
	graphsz = 400
	# Our sample size is 5 seconds
	sample_rate = 5
	# Steps that represent 1 day on the graph (entire graph will be 1 day)
	steps_day = calc_steps(1, sample_rate, graphsz)
	# Steps that represent 7 days on the graph (entire graph represents 1 week)
	steps_week = calc_steps(7, sample_rate, graphsz)
	# Steps that represent 30 days on the graph (entire graph represents 1 month)
	steps_month = calc_steps(30, sample_rate, graphsz)
	# X-Files Factor - the number of data points that can be anally probed by martians before RRD gives a crap.
	# (basically how many "unknown" data points happen before the graph becomes unusable (I think))
	xff = 0.5 # 0.5 means "half the data points"
	# tuple representing this all
	rras = (
		# This is a new database starting from "now"
		"--start", "now",
		# We expect a new sample every 5 seconds
		"--step", f"{sample_rate}",
		# The Data sources we have
		# DS    = DataSource (keyword)
		# ev_d  = reference variable
		# GAUGE = don't do extra calculations on input data
		# 10    = heartbeat - how often we sample (plus slack) in seconds
		# 0     = min - minimum value to consider valid
		# 0     = max - maximum value to consider valid (In this case, unknown max)
		f"DS:ev_d:GAUGE:{sample_rate * 2}:0:U",
		# Our Round Robin Archive statements
		f"RRA:MIN:{xff}:{steps_month}:{graphsz}",
		f"RRA:MIN:{xff}:{steps_week}:{graphsz}",
		f"RRA:MIN:{xff}:{steps_day}:{graphsz}",
		f"RRA:AVERAGE:{xff}:{steps_month}:{graphsz}",
		f"RRA:AVERAGE:{xff}:{steps_week}:{graphsz}",
		f"RRA:AVERAGE:{xff}:{steps_day}:{graphsz}",
		f"RRA:MAX:{xff}:{steps_month}:{graphsz}",
		f"RRA:MAX:{xff}:{steps_week}:{graphsz}",
		f"RRA:MAX:{xff}:{steps_day}:{graphsz}"
	)
	# Create our files
	# TODO: make this one db file?
	if not Path(RRD_DB_DAY).is_file():
		rrdtool.create(RRD_DB_DAY, *rras)
		# rrdtool.create(RRD_DB_DAY, "--start", "now", "--step", "5", f"RRA:AVERAGE:0:1:{steps_day}", "DS:ev_d:GAUGE:10:0:30")
	# if not Path(RRD_DB_WEEK).is_file():
	# 	rrdtool.create(RRD_DB_WEEK, "--start", "now", "--step", "5", f"RRA:AVERAGE:0:1:{steps_week}", "DS:events:GAUGE:10:0:30")
	# if not Path(RRD_DB_MONTH).is_file():
	# 	rrdtool.create(RRD_DB_MONTH, "--start", "now", "--step", "5", f"RRA:AVERAGE:0:1:{steps_month}", "DS:events:GAUGE:10:0:50")


@Client.on_message(~filters.sticker & ~filters.via_bot & filters.me & filters.command(['fspam'], prefixes=config['config']['prefixes']))
@log_errors
async def fspam(client, message):
	if not getattr(message.reply_to_message, 'empty', True):
		for chat in spamreportchats:
			await client.forward_messages(chat_id=chat, from_chat_id=message.reply_to_message.chat.id, message_ids=[message.reply_to_message.id], disable_notification=True)
	await message.delete()

@Client.on_message(~filters.sticker & ~filters.via_bot & filters.me & filters.command(['log'], prefixes=config['config']['prefixes']))
@log_errors
async def log_to_logchat(client, message):
	if not getattr(message.reply_to_message, 'empty', True):
		await client.forward_messages(chat_id=config['logging']['regular'], from_chat_id=message.reply_to_message.chat.id, message_ids=[message.reply_to_message.id], disable_notification=True)
	await message.delete()

@on_event("OnAddedUser")
async def MassAddTracker(_, EventName: str, client, user, chat, added, message_id):
	if not config['config']['massadd']['enabled']:
		return

	# Creating new users will cause duplicate entries to start
	# so lock to prevent weird shit from happening.
	async with dblock:

		# First query that anything exists at all.
		adder = await MAUser.objects.get_or_none(id=user.id)
		seen_account_id = next((key for key,val in apps.items() if val == client), None)

		if not adder:
			encoded_data = ziproto.encode({
				AdderBlob.ADDER_SEENACT: seen_account_id
			})
			adder = MAUser(id=user.id, first_name=user.first_name, last_name=user.last_name, username=user.username, metadata=encoded_data)
			await adder.save()
		else:
			# update the record.
			adder.record_updated = datetime.now()
			# Check if anything changed
			if adder.first_name != user.first_name:
				adder.first_name = user.first_name
			if adder.last_name != user.last_name:
				adder.last_name = user.last_name
			if adder.username != user.username:
				adder.username = user.username
			await adder.update()

		# Check if the chat exists.
		c = await MAChatroom.objects.get_or_none(id=chat.id)
		if not c:
			c = MAChatroom(id=chat.id, username=chat.username, title=chat.title)
			await c.save()
		else:
			# update the record
			c.record_updated = datetime.now()
			# Check if anything changed
			if c.title != chat.title:
				c.title = chat.title
			if c.username != chat.username:
				c.username = chat.username
			await c.update()

		event = MAEvent(uuid=uuid.uuid4(), chat=c, aps=0.0)
		await event.save()

		# ok lets find and update the users.
		for a in added:
			# Skip bots, we don't care if people add a million bots.
			if a.bot:
				continue

			# Get the added user
			au = await MAUser.objects.get_or_none(id=a.id)
			# If they don't exist, create their record
			if not au:
				encoded_data = ziproto.encode({
					AdderBlob.ADDER_SEENACT: seen_account_id
				})
				au = MAUser(id=a.id, first_name=a.first_name, last_name=a.last_name, username=a.username, metadata=encoded_data)
				await au.save()
			else:
				# update the record.
				au.record_updated = datetime.now()
				# Check if anything changed
				if au.first_name != a.first_name:
					au.first_name = a.first_name
				if au.last_name != a.last_name:
					au.last_name = a.last_name
				if au.username != a.username:
					au.username = a.username
				await au.update()
			
			# Create a new added user record for this specific event
			# and this specific user added to this specific chat
			ar = MAAddRecord(event=event, adder=adder, added=au)
			await ar.save()

	# Calculate the mass-add event tracking
	async with exp_lock:
		if not chat.id in active_events:
			active_events[chat.id] = {
				# Event ID
				'uuid': event.uuid,
				# This determines when the event is over.
				'load': 0.0,
				# Track the adds per second
				'aps': [],
				# add count this iteration (for load above)
				'add_cnt': len(added),
				# add total during event
				'total_cnt': len(added),
				# Chat object (so we know wtf this chat is)
				'chatobj': chat,
				# The event object
				'eventobj': event,
				# First start of the spam-add
				'message_id': message_id,
				# Spam adders participating, including adder obj and total they've added
				# during this event
				'participants': {
					user.id: (user, len(added))
				},
				# the message we've sent to the log chat (so we can repeatedly edit it)
				'msg': None,
				# when the event began.
				'date': datetime.now(),
				# if the message is too big, send as file instead of editing.
				'toobig': False
			}

			await emit("OnNewMassAdd", block=False, event_data=active_events[chat.id])

		else:
			# update the counts
			active_events[chat.id]['add_cnt'] += len(added)
			active_events[chat.id]['total_cnt'] += len(added)
			# new participant?
			parts = active_events[chat.id]['participants']
			if user.id not in parts:
				active_events[chat.id]['participants'][user.id] = (user, len(added))
			else:
				idk = active_events[chat.id]['participants'][user.id]
				active_events[chat.id]['participants'][user.id] = (idk[0], idk[1] + len(added))
			await emit("OnMassAddUpdate", block=False, event_data=active_events[chat.id])

# Generate a report on the specific event
async def ma_event(client, message, args):

	if not args:
		await self_destruct("<code>You must specify an ID</code>")
		return

	def _event_func(client: Client, message: Message, args):
		report_uuid = args[0]

		# immediately query for the event, we'll need to gather some other shit as well.
		reply = message.reply("Generating results, please wait...")

		ch = asyncio.run_coroutine_threadsafe(MAEvent.objects.get_or_none(uuid=report_uuid), loop).result()
		if not ch:
			reply.edit("Event does not exist.")
			return

		ars = asyncio.run_coroutine_threadsafe(MAAddRecord.objects.filter(event=ch), loop).result()
		cnt = asyncio.run_coroutine_threadsafe(ars.count(), loop).result()

		participants = {}
		for ar in ars:
			if not ar.adder.id in participants:
				participants[ar.adder.id] = []

			participants[ar.adder.id].append({
				"id": ar.added.id,
				"first_name": ar.added.first_name,
				"last_name": ar.added.last_name,
				"username": ar.added.username,
			})

		# start the json array
		jsob_resp = {
			# Event information
			"event": {
				"uuid": ch.uuid,
				"chatid": ch.chat.id,
				"aps": ch.aps,
				"begin": ch.begin_time,
				"end": ch.end_time,
				"added_count": cnt
			},
			# people who participated in the events.
			"participants": participants
		}

		f = BytesIO(json.dumps(jsob_resp, indent=4).strip().encode('utf-8'))
		f.name = f"massadd_event_{ch.uuid}.json"
		message.reply_document(f, caption=f"This event has a total of {cnt} added users with {len(jsob_resp['participants'].keys())} participants. See attached json for full details.")
		reply.delete()

	# Throw it into a thread, make the thread deal with it.
	threading.Thread(target=_event_func, args=(client, message, args)).start()

# Generate a report on the specific user
async def ma_adder(client, message, args):

	if not args:
		await self_destruct("<code>You must specify an ID</code>")
		return

	def _adder_func(client: Client, message: Message, args):
		addr_id = args[0]

		reply = message.reply("Please wait, generating results...")

		# Start by querying the database for the adder.
		ch = asyncio.run_coroutine_threadsafe(MAUser.objects.get_or_none(id=addr_id), loop).result()
		# ch = asyncio.run_coroutine_threadsafe(localsession.execute(select(Adder).where(Adder.id == addr_id)), loop).result().scalar_one_or_none()
		if not ch:
			reply.edit("This user has not added anyone.")
			return

		# Query for all MAAddRecords
		ars = asyncio.run_coroutine_threadsafe(MAAddRecord.objects.filter(adder=ch), loop).result()
		cnt = asyncio.run_coroutine_threadsafe(ars.count(), loop).result()

		events = {}
		groups = []
		for ar in ars:
			# Now handle events
			if ar.event.uuid not in events:
				if not ar.event.chat.id in groups:
					groups.append(ar.event.chat.id)
				events[ar.event.uuid] = {
					"chat_id": ar.event.chat.id,
					"title": ar.event.chat.title,
					"begin": ar.event.begin,
					"end": ar.event.end,
					"added": [],
					"added_count": 0
				}
			events[ar.event.uuid]["added_count"] += 1
			events[ar.event.uuid]["added"].append({
				"user_id": ar.added.id,
				"username": ar.added.username,
				"first_name": ar.added.first_name,
				"last_name": ar.added.last_name
			})

		# begin our json dictionary for info.
		jsonresp = {
			# Info about the spam-adder
			"adderinfo": {
				"id": ch.id,
				"first_name": ch.first_name,
				"last_name": ch.last_name,
				"username": ch.username,
				"record_creation": ch.record_creation,
				"record_updated": ch.record_updated,
				"total_added": cnt,
				"total_events": len(events.keys()),
				"total_groups": len(groups)
			},
			# List of events they participated in and what users were added
			# in each event
			"events": events
		}

		f = BytesIO(json.dumps(jsonresp, indent=4).strip().encode('utf-8'))
		f.name = f"massadd_report_{addr_id}.json"
		message.reply_document(f, caption=f"User added {cnt} user(s) in {len(events.keys())} event(s) across {len(groups)} group(s)")
		reply.delete()

	# Throw it into a thread, make the thread deal with it.
	threading.Thread(target=_adder_func, args=(client, message, args)).start()

@on_event("OnStatistics")
async def handle_stats(_, EventName: str):
	# text = textproxy.get_thing()
	text = "<b>Mass-Add Statistics</b>\n"
	text += f" - Active Events: <code>{len(active_events.keys())}</code>\n"

	# Run queries to get current counts
	users = await MAUser.objects.count()
	events = await MAEvent.objects.count()
	groups = await MAChatroom.objects.count()
	records = await MAAddRecord.objects.count()

	# TODO: find a solution for adder counts
	# text += f" - Total Adders: <code>{adders}</code>\n"
	text += f" - Total Events: <code>{events}</code>\n"
	text += f" - Total Users: <code>{users}</code>\n"
	text += f" - Total Groups: <code>{groups}</code>\n\n"
	text += f" - Total Records: <code>{records}</code>\n\n"
	return text
	# textproxy.set_thing(text)

# Generate a report on a specific group
async def ma_group(client, message, args):
	if not args:
		await self_destruct(message, "<code>You must specify an ID</code>")
		return

	# In this function, we basically differ entirely to a thread and use
	# some thread-safe coroutine calls.
	def _find_events(client, message, args):
		global loop
		grp = args[0]

		# Things get much more tricky with threading, every await call becomes `asyncio.run_coroutine_threadsafe`
		# and we must wait for the future to complete to get the awaited response.
		ch = asyncio.run_coroutine_threadsafe(MAChatroom.objects.get_or_none(id=int(grp)), loop).result()
		if not ch:
			message.reply(f"No information for <code>{grp}</code>")
			return

		reply = message.reply(f"Generating results for <code>{name_escape(ch.title)} [{ch.id}]</code>")

		# now get all participants in all these events
		all_records = asyncio.run_coroutine_threadsafe(MAAddRecord.objects.filter(event__chat=ch), loop).result()

		json_resp = {
			"id": ch.id,
			"username": ch.username,
			"title": ch.title,
			"record_created": ch.record_creation,
			"record_updated": ch.record_updated,
			"events": {}
		}

		event_cnt = 0
		added_cnt = 0
		adder_cnt = 0

		for record in all_records:
			if not record.event.uuid in json_resp["events"]:
				event_cnt += 1
				json_resp["events"][record.event.uuid] = {
					"aps": record.event.aps,
					"begin": record.event.begin_time,
					"end": record.event.end_time,
					"participants": {}
				}
			if not record.adder.id in json_resp["events"][record.event.uuid]["participants"]:
				adder_cnt += 1
				json_resp["events"][record.event.uuid]["participants"][record.adder.id] = {
					"first_name": record.adder.first_name,
					"last_name": record.adder.last_name,
					"username": record.adder.username,
					"added": [],
					"added_count": 0
				}
			added_cnt += 1
			json_resp["events"][record.event.uuid]["participants"][record.adder.id]["added_count"] += 1
			json_resp["events"][record.event.uuid]["participants"][record.adder.id]["added"].append({
				"id": record.added.id,
				"first_name": record.added.first_name,
				"last_name": record.added.last_name,
				"username": record.added.username
			})

		f = BytesIO(json.dumps(json_resp, indent=4).strip().encode('utf-8'))
		f.name = f"massadd_group_{ch.id}.json"
		message.reply_document(f, caption=f"Chat has a total of {event_cnt} events with {adder_cnt} participants adding a total of {added_cnt} users. See the attached file for details.")
		reply.delete()

	# Make the thread handle everything now and we can continue doing other stuff.
	threading.Thread(target=_find_events, args=(client, message, args)).start()
	# just return, we don't care anymore.

# Query all IDs in an event and blacklist them
# then return a list of PTIDs that were blacklisted.
async def ma_blacklist(client, message, args):
	if not args:
		await self_destruct(message, "<code>You must specify an ID</code>")
		return

	from shadowhawk import plmgr as PluginManager
	plug = PluginManager.FindPlugin("spamsystemcontrol")

	if plug:
		# Multiple IDs can be banned.
		for report_uuid in args:
			await plug.module.blacklist_event(client, report_uuid, automated=False)
		
		await message.reply("Command Operation Complete.")
	else:
		await message.reply("The <code>spamsystemcontrol</code> is not loaded!")

# List the current mass-add events we're apart of
async def ma_current(client, message, args):
	msgtext = f"<b>Active Mass-Add Events ({len(active_events.keys())})</b>\n"
	async with exp_lock:
		if not active_events:
			msgtext += "<i>No active events.</i>"
		for id, details in active_events.items():
			delta = datetime.now() - details['date']
			link = ""
			if details['msg']:
				link = await get_chat_link(client, details['msg'])
			msgtext += f"- <b><a href=\"{link}\">Event:</a></b> <code>{details['uuid']}</code>\n"
			msgtext += f"-- APS: {mean(details['aps']):.3f} ({details['load']:.3f})  [t: {details['total_cnt']}/p: {len(details['participants'])}]\n"
			msgtext += f"-- Duration: {get_duration(delta.total_seconds())}\n"
	await message.reply(msgtext)

async def ma_tracking(client, message, args):
	if not args:
		await message.reply("on or off, pick one.")
		return

	if args[0].lower() == "on":
		config['config']['massadd']['enabled'] == True
	elif args[0].lower() == "off":
		config['config']['massadd']['enabled'] = False
	else:
		await message.reply("on or off, pick one.")
		return

	await message.reply(f"Tracking enabled: {config['config']['massadd']['enabled']}")

async def ma_graph(client, message, args):
	o = rrdtool.graphv("-", "-a", "PNG", "--title", "Mass-Add Events (day)", "--vertical-label", "Events", "DEF:ev_d=sessions/massadd.rrd:ev_d:AVERAGE", "LINE:ev_d#FF0000:Events")
	out = BytesIO(o['image'])
	out.name = "graph.png"
	await message.reply_photo(out)

async def ma_help(client, message, args):
	prefixes = config['config']['prefixes']
	prefix = prefixes[0]
	help = f"""Help for Antispam:
Avaliable prefixes: {", ".join(prefixes)}

{prefix}massadd help - Display this message
Aliases: {prefix}ma help

{prefix}massadd event <i>&lt;event uuid&gt;</i> - Generate a report on an event
Aliases: {prefix}ma event

{prefix}massadd adder <i>&lt;peer id&gt;</i> - Generate a report on an adder
Aliases: {prefix}ma adder

{prefix}massadd group <i>&lt;peer id&gt;</i> - Generate a report on a group
Aliases: {prefix}ma group

{prefix}massadd current - Display active mass-add events currently being tracked
Aliases: {prefix}ma current

{prefix}massadd graph - Display a graph showing the mass-add events for the last 24h
Aliases: {prefix}ma graph

<b>Owner only commands:</b>

{prefix}massadd blacklist <i>&lt;event uuid&gt;</i> - Begin the blacklisting process for @SpamProtectionBot
Aliases: {prefix}ma blacklist

{prefix}massadd tracking <i>&lt;event uuid&gt;</i> - Temporarily disable or enable mass-add event tracking
Aliases: {prefix}ma tracking
"""
	await message.reply(help, disable_web_page_preview=True)

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & (filters.me | sudoers.sudo) & filters.command(['ma', 'massadd'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def massadd(client, message):
	command = message.command
	command.pop(0)
	subcmd = command.pop(0).lower()

	is_sudoer = await sudoers.is_sudo(message.chat, message.from_user)

	if "help".startswith(subcmd):
		await ma_help(client,message, command)
	if "event".startswith(subcmd):
		await ma_event(client, message, command)
	elif "adder".startswith(subcmd):
		await ma_adder(client, message, command)
	elif "group".startswith(subcmd):
		await ma_group(client, message, command)
	elif "current".startswith(subcmd):
		await ma_current(client, message, command)
	elif "graph".startswith(subcmd):
		await ma_graph(client, message, command)

	# Sudoers can't execute these commands
	if not is_sudoer:
		if "blacklist".startswith(subcmd):
			await ma_blacklist(client, message, command)
		elif "tracking".startswith(subcmd):
			await ma_tracking(client, message, command)

# Crypto spammers tend to use these unicode ranges to delete messages
# The ranges observed are:
# Phonetic Extensions, 0x1D00 - 0x1D7F
# Mathematical Alphanumeric Symbols, U+1D400 - U+1D7FF
# @Client.on_message(filters.text & ~filters.chat([config['logging']['regular'], config['logging']['spammy']]) & filters.group)
async def spammer_delete(client, message):
	# Check fi the chars are in the range, normally I'd use regex for this but
	# that is not really the case
	score = 0
	for ch in message.text:
		asc = ord(ch)
		if asc <= 0x1D7F and asc >= 0x1D00:
			score += 1
		if asc <= 0x1D7FF and asc >= 0x1D400:
			score += 1

	if score > 50:
		# Do some magic trickery to avoid being banned by telegram or limited by @SpamInfo
		peerid = (await client.resolve_peer(message.chat.id)).channel_id
		link = f"https://t.me/c/{peerid}/{message.id}"
		reply = await client.send_message(config['logging']['spammy'], message.text.markdown)
		# reply = await message.forward(config['logging']['regular'])
		await reply.reply(f"#UNICODESPAM {link}")
	raise ContinuePropagation

__signature__ = "SHSIG-INjGd/O66GrVIaCE0qCHpBdDM/aRpD0yb+eVzv67PnEhAAAAIJRw7oS2Yo85gMELov0bi2NSQMQdE07etaefbDq562u5AAAA"