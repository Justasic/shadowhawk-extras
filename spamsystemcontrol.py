import ormar
import ziproto
import asyncio
import traceback
from uuid import UUID
from asyncevents import on_event
from pyrogram import Client, filters
from shadowhawk import config
from shadowhawk import plmgr as PluginManager
from shadowhawk.utils import get_app
from shadowhawk.utils.FileUtils import return_progress_string
from shadowhawk.database import BaseModel

from shadowhawk.plugins.justasic.antispam import MAEvent, MAUser, MAAddRecord

conversation_hack = {}

class PendingActions(ormar.Model):
	class Meta(BaseModel):
		tablename = 'PendingActions'

	# Id of this row
	id: int = ormar.BigInteger(primary_key=True, auto_increment=True)
	# UUID of the event that needs to be done
	uuid: UUID = ormar.UUID()
	# TODO: support multiple actions somehow??

async def blacklist_event(client, event_uuid, message=None, automated=True):
	# Make a reply for us to work with
	if not message:
		reply = message = await client.send_message(config['config']['massadd']['log_location'], f"Generating results for <code>{event_uuid}</code>, please wait...")
	else:
		reply = await message.reply(f"Generating results for <code>{event_uuid}</code>, please wait...")

	# immediately query for the event, we'll need to gather some other shit as well.
	ch = await MAEvent.objects.get_or_none(uuid=event_uuid)
	if not ch:
		await reply.edit(f"Event <code>{event_uuid}</code> does not exist.")
		return

	# Get all the adders and make an array for them
	event_ars = await MAAddRecord.objects.filter(event=ch)
	adders = []
	for ar in event_ars:
		if ar.adder.id not in adders:
			adders.append(ar.adder.id)

	# Now get all the adder's records and make an array for totals
	ars = await MAAddRecord.objects.filter(adder__id__in=adders)
	adders = {}
	for ar in ars:
		if not ar.adder.id in adders:
			adders[ar.adder.id] = {
				"events": [],
				"chats": [],
				"added_cnt": 0
			}
		adders[ar.adder.id] += 1
		if not ar.event.chat.id in adders[ar.adder.id]["chats"]:
			adders[ar.adder.id].append(ar.event.chat.id)
		if not ar.event.uuid in adders[ar.adder.id]["events"]:
			adders[ar.adder.id]["events"].append(ar.event.uuid)

	# Ban in Sibyl
	plug = PluginManager.FindPlugin("sibyl")
	print(plug)
	if plug:
		sibyl_client = plug.sibyl_client
		if sibyl_client:
			multibans = []
			for adder_id, adder_info in adders.items():
				event_cnt = len(adder_info["events"])
				cnt = len(adder_info["added_cnt"])
				chat_cnt = len(adder_info["chats"])
				# Don't blacklist if their ratio is less than or equal to
				# 500%, this is likely to be a legitmate user who has added
				# many people over time and we've just observed them. Spam
				# adders are unlikely to add this slowly. I may adjust this
				# ratio if innocent people get caught up in this.
				# Also don't blacklist if their total added count isn't above 25 members
				if ((cnt/event_cnt) * 100.0 <= 500.0) or (cnt < 25):
					continue

				# Query sibyl if they're banned already
				info = sibyl_client.get_info(adder_id)
				if not info.banned:
					# Create a ban.
					from SibylSystem.types import MultiBanInfo
					multibans.append(MultiBanInfo(user_id=adder_id, reason=f"Mass-Add {cnt} users across {event_cnt} event(s) in {chat_cnt} chat(s)"))
			
			# Send the ban request to sibyl
			if multibans:
				txt = sibyl_client.multi_ban(multibans)
				await reply.edit(f"Response from sibyl to ban {len(adders.keys())} users: <code>{txt}</code>")
			else:
				await reply.edit(f"No blacklistable users for <code>{event_uuid}</code>")

		# avoid floodwaits.
		await asyncio.sleep(5)

	# if there's been many mass-add events in this chat, perform an association ban.
	await message.reply("Event Operation Complete.")

# Handle doing pending actions
async def process_actions():
	while True:
		try:
			# Don't continue the loop if automatic_action's are disabled
			if not config['config']['massadd']['automatic_action']:
				return
			# get one object from the DB
			action = await PendingActions.objects.get_or_none()
			action_account = await get_app(config['config']['massadd']['action_account'])
			if action:
				print(f"Processing action for {action.uuid}")
				await blacklist_event(action_account, str(action.uuid), automated=True)
				await action.delete()
		except BaseException:
			traceback.print_exc()
		await asyncio.sleep(5)

@on_event("OnMassAddComplete")
async def CreateAction(_, EventName: str, event: MAEvent):

	# Save an event action if actions are enabled
	if config['config']['massadd']['automatic_action']:
		await PendingActions(uuid=UUID(event.uuid)).save()

__signature__ = "SHSIG-IEyVTXwYvM/iXNnFTcFtkpSBTa880qXZ90rxPamQLzzTAAAAIOqrePkNmtP3xfRqn6iMyhATLga1DlTWHA4riJOeJ3p3AAAA"