import html, ormar
# from typing import Text
from pyrogram import Client, filters
from datetime import datetime, timedelta
from durations import Duration
from asyncevents import on_event
from pyrogram.types import Message
from shadowhawk import config, acm
from shadowhawk.utils import build_name_flags, get_entity, self_destruct, get_id_from_app
from shadowhawk.utils.Administration import is_admin
from shadowhawk.utils.Command import parse_command
from shadowhawk.utils.Logging import log_errors, public_log_errors, log_chat
from shadowhawk.plugins.moderation import ResolveChatUser
from shadowhawk.database import BaseModel

# A global cache of banned users in chats, loaded at start
EnforcedBansCache = {}

class EnforcedBan(ormar.Model):
	class Meta(BaseModel):
		tablename = 'EnforcedBans'
	
	id: int = ormar.BigInteger(primary_key=True)
	chat_id: int = ormar.BigInteger()
	user_id: int = ormar.BigInteger()
	reason: str = ormar.Text(nullable=True)


@on_event("OnDatabaseStart")
async def OnDBStart(_, EventName: str):
	global EnforcedBansCache

	banned = await EnforcedBan.objects.all()
	for user in banned:
		if user.chat_id not in EnforcedBansCache:
			EnforcedBansCache[user.chat_id] = []
		EnforcedBansCache[user.chat_id].append(user.user_id)


@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['lsenforcedbans', 'lsebans'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def ls_permbans(client: Client, message: Message):
	ebans = await EnforcedBan.objects.all()
	text = "<b>List of Enforced Bans</b>\n"
	text_unping = text
	for s in ebans:
		su, _ = await get_entity(client, s[0].id)
		text += f"- <a href=\"tg://user?id={s[0].id}\">{su.first_name} {su.last_name}</a> <code>[{s[0].id}]</code>\n"
		text_unping += f"- {su.first_name} {su.last_name} <code>[{s[0].id}]</code>\n"

	msg = await message.reply(text_unping, disable_web_page_preview=True)
	msg = await msg.edit(text, disable_web_page_preview=True)

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['eban', 'enforcedban'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def add_eban(client: Client, message: Message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)
	time = next((val for key,val in owo.items() if key in ['t', 'time']), None)
	uhh = None

	if time:
		uhh = Duration(time)
		time = datetime.now() + timedelta(seconds=uhh.to_seconds())

	if not await client.kick_chat_member(chat_id=chat.id, user_id=user.id, until_date=time):
		await self_destruct(message, "<code>I cannot mute that.</code>")
		return

	## Add a new record to the ban table
	# First, query the DB, we may be adding more chats to this user.
	sql_user = await EnforcedBan.objects.get_or_none(user_id=user.id)

	# ban the user if they're banned, update the reason otherwise.
	if not sql_user:
		sql_user = EnforcedBan(chat.id, user.id, reason)
	else:
		sql_user.reason = reason

	await sql_user.save() 

	# Add to the cached entities
	if chat.id not in EnforcedBansCache:
		EnforcedBansCache[chat.id] = []
	if user.id not in EnforcedBansCache[chat.id]:
		EnforcedBansCache[chat.id].append(user.id)

	# Delete the command message
	await message.delete()

	# log if we successfully kicked someone.
	chat_text = '<b>New Enforced Ban Event</b> [#NEWENFORCEDBAN]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat.id)
	chat_text += '\n- <b>Ban:</b> ' + await build_name_flags(client, user.id)
	chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)


@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['uneban', 'unenforcedban', 'rmenforcedban', 'rmeban'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def rm_eban(client: Client, message: Message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)
	time = next((val for key,val in owo.items() if key in ['t', 'time']), None)
	uhh = None

	if time:
		uhh = Duration(time)
		time = datetime.now() + timedelta(seconds=uhh.to_seconds())

	if not await client.unban_chat_member(chat_id=chat.id, user_id=user.id, until_date=time):
		await self_destruct(message, "<code>I cannot mute that.</code>")
		return

	## Add a new record to the ban table
	# First, query the DB, we may be adding more chats to this user.
	sql_user = await EnforcedBan.objects.get_or_none(user_id=message.from_user.id, chat_id=chat.id)

	# ban the user if they're banned, update the reason otherwise.
	if sql_user:
		await sql_user.delete()
		del EnforcedBansCache[chat.id][user.id]
		if not EnforcedBansCache[chat.id]:
			del EnforcedBansCache[chat.id]

	# log if we successfully kicked someone.
	chat_text = '<b>Enforced Ban Removal Event</b> [#REMOVEENFORCEDBAN]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat.id)
	chat_text += '\n- <b>Ban:</b> ' + await build_name_flags(client, user.id)
	chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)


@Client.on_chat_member_updated(filters.all & filters.group)
@log_errors
async def enforce_ban(client, message):
	chat = message.chat
	if chat.id in EnforcedBansCache:
		if message.from_user.id in EnforcedBansCache[chat.id]:
			# user was unbanned somehow, enforce the ban.
			await client.kick_chat_member(chat_id=chat.id, user_id=message.from_user.id)

			# Set a default reason
			reason = "Unknown."

			# We need to query the database for a reason.
			sql_user = await EnforcedBan.objects.get_or_none(user_id=message.from_user.id, chat_id=chat.id)
			if sql_user:
				if sql_user.reason:
					reason = sql_user.reason

			chat_text = '<b>Enforced Ban Event</b> [#ENFORCEDBAN]'
			chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat.id)
			chat_text += '\n- <b>Ban:</b> ' + await build_name_flags(client, message.from_user.id)
			chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

			await log_chat(chat_text)

__signature__ = "SHSIG-IAVwk+kckfmJdkchTIoUvMPUYggCIJWYJoYCAsuxIY3oAAAAIJySuJrhxomj4p2v/xYx3w3o9NdUdeOsn8AY+9NpObMnAAAA"