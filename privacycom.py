import pytz
import html
import asyncio
import privacy
import time
import zlib
import ormar
from privacy.schema.cards import State
from privacy.schema.transactions import Status
from dateutil.parser import parse
from babel import numbers
from aiohttp import web
from pyrogram import Client, filters
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton, InlineQueryResultArticle, InputTextMessageContent
from shadowhawk.database import BaseModel
from shadowhawk.utils import self_destruct, get_entity, get_app
from shadowhawk.utils.Logging import log_errors, public_log_errors
from shadowhawk import config, app_user_ids, slave

__help_section__ = "Privacy.com"

if config['config']['privacycom']:
	if config['config']['privacycom']['production']:
		pcli = privacy.Client(config['config']['privacycom']['production'], sandboxed=False)
	else:
		pcli = None

	if config['config']['privacycom']['testing']:
		pcli_dbg = privacy.Client(config['config']['privacycom']['testing'], sandboxed=True)
	else:
		pcli_dbg = None
app = web.Application()

class PrivacyComAnnounce(ormar.Model):
	class Meta(BaseModel):
		tablename = 'PrivacyComAnnounce'

	id: int =  ormar.Integer(primary_key=True, autoincrement=True)
	channel_id: id = ormar.BigInteger()
	token: str = ormar.Text()

@log_errors
async def index(request):
	if request.content_type == 'application/json':
		payload = await request.json()

		print(payload['card'])
		if not payload.get('card'):
			print("Ignoring unknown event, dumping json below:")
			print(payload)
			return web.Response(body="OK", status=200)
		# fae1ffbb-7fa0-4fb7-8107-5e5e7254586e
		lel = await PrivacyComAnnounce.objects.filter(token=payload.get('card').get('token')).all()
		if not lel:
			print("Ignoring transaction announce for card " + payload['card']['token'])
			return web.Response(body="OK", status=200)

		tz = pytz.timezone(config['config']['timezone'])

		transinfo = "\N{BANKNOTE WITH DOLLAR SIGN} New Transaction\n"
		hashtag = "#transaction"
		if payload.get('amount'):
			amount = numbers.format_currency(int(payload['amount']) / 100, "USD", locale='en')
			transinfo += f"<b>Amount</b>: {amount}\n"
		if payload.get('settled_amount'):
			settled_amount = numbers.format_currency(int(payload['settled_amount']) / 100, "USD", locale='en')
			transinfo += f"<b>Settled</b>: {settled_amount}\n"
		if payload.get('status'):
			emoji = "\N{NO ENTRY}" if payload['status'] == "DECLINED" else "\N{WHITE HEAVY CHECK MARK}"
			transinfo += f"<b>Status</b>: {payload['status']} {emoji}\n"
			hashtag += f" #{payload['status'].lower()}"
		if payload.get('result'):
			transinfo += f"<b>Result</b>: {payload.get('result')}\n"
			hashtag += f" #{payload['result'].lower()}"
		if payload.get('merchant') and payload.get('merchant').get('descriptor'):
			transinfo += f"<b>Merchant</b>: {payload.get('merchant').get('descriptor')}\n"
		if payload.get('created'):
			date = parse(payload['created']).astimezone(tz).strftime("%c")
			transinfo += f"<b>Date</b>: {date}\n"
		if payload.get('token'):
			transinfo += f"<b>Trans Token</b>: <code>{payload.get('token')}</code>\n"
		if payload.get('card') and payload.get('card').get('token'):
			transinfo += f"<b>Card Token</b>: <code>{payload.get('card').get('token')}</code>\n"

		transinfo += hashtag + "\n"

		# Now we must find the client that has access to the chat.
		for ch in lel:
			# Attempt to get the primary client for announcements
			app = await get_app(config['config']['privacycom']['announce_account'])
			# Try and find the chat with the primary client
			chat, client = await get_entity(app, ch.channel_id)
			# Send the message with (hopefully) the primary client.
			await client.send_message(chat.id, transinfo)

	return web.Response(body="OK", status=200)

# https://stackoverflow.com/questions/53465862/python-aiohttp-into-existing-event-loop
async def httpd():
	# Add the one route we need
	app.router.add_post('/api/', index)
	# Begin the webserver startup
	runner = web.AppRunner(app)
	await runner.setup()
	site = web.TCPSite(runner, port=8633)
	await site.start()
	# wait forever.
	await asyncio.Event().wait()

asyncio.create_task(httpd())

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['listcards', 'cardlist'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def listcards(client, message: Message):
	"""{prefix}cardlist <i>[-d]</i> - Lists Privacy.com cards (-d for debug session)"""
	# -cardlist -d
	dbg = "-d" in message.command

	if dbg:
		cards = pcli_dbg.cards_list()
	else:
		cards = pcli.cards_list()

	opencards = "<b>The following cards are active</b>:\n"
	pausedcards = "<b>The following cards are paused</b>:\n"
	haveopen = False
	havepaused = False
	for c in cards:
		if c.state == State.OPEN:
			haveopen = True
			opencards += f"\N{Bullet} <b>{c.last_four}</b>: <code>{c.token}</code>\n"
			if c.memo:
				opencards += f"\N{BOX DRAWINGS LIGHT UP AND RIGHT}\N{BOX DRAWINGS LIGHT HORIZONTAL} <b>Memo:</b> {c.memo}\n"
		if c.state == State.PAUSED:
			havepaused = True
			pausedcards += f"\N{Bullet} <b>{c.last_four}</b>: <code>{c.token}</code>\n"
			if c.memo:
				pausedcards += f"\N{BOX DRAWINGS LIGHT UP AND RIGHT}\N{BOX DRAWINGS LIGHT HORIZONTAL} <b>Memo:</b> {c.memo}\n"

	if haveopen and havepaused:
		cardlist = f"{opencards}\n{pausedcards}"
	elif haveopen and not havepaused:
		cardlist = opencards
	elif not haveopen and havepaused:
		cardlist = pausedcards
	else:
		cardlist = "<b>There are no open cards.</b>"

	await message.reply(cardlist)

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['cardinfo'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def cardinfo(client, message: Message):
	"""{prefix}cardinfo <i>[-d] &lt;card token&gt;</i> - Print info on a card (-d for debug session)"""
	# -cardinfo -d <card token>
	command = message.command
	command.pop(0)
	if command:
		if command[0] == "-d":
			card = pcli_dbg.cards_list(token=command[1])
		else:
			card = pcli.cards_list(token=command[0])

		# incrediblt dumb but what the fuck ever.
		card.set_direction()
		card = next(card, None)

		if card:
			cinfo = f"<b>Card</b> <code>{card.token}</code>\n"
			if card.memo:
				cinfo += f"<b>Memo:</b> {card.memo}\n"
			if card.hostname:
				cinfo += f"<b>Hostname:</b> {card.hostname}\n"
			if card.pan:
				cinfo += f"<b>PAN:</b> {card.pan}\n"
			elif card.last_four:
				cinfo += f"<b>PAN:</b> **{card.last_four}\n"
			if card.exp_month and card.exp_year:
				cinfo += f"<b>EXP:</b> {card.exp_month}/{card.exp_year}"
				if card.cvv:
					cinfo += f" <b>CVV:</b> {card.cvv}"
				cinfo += "\n"
			if card.spend_limit:
				limit = int(card.spend_limit)/100
				cinfo += f"<b>Limit:</b> ${limit:,.2f} ({card.spend_limit_duration.value})\n"
			if card.state:
				cinfo += f"<b>State:</b> {card.state.value}\n"
			if card.type:
				cinfo += f"<b>Type:</b> {card.type.value}\n"
			await message.reply(cinfo)
			return
	await self_destruct(message, "<code>You must specify a valid token</code>")

message_lock = asyncio.Lock()
privacycom_queries = {}

@slave.on_inline_query(filters.regex('^engine_pctrans-(\d+)$'))
@log_errors
async def translist_start(client, inline_query):
	async with message_lock:
		target = int(inline_query.matches[0].group(1))

		answers = []
		msgs, page  = privacycom_queries[target]
		for a, result in enumerate(msgs):
			buttons = [
				InlineKeyboardButton('Back', f'engine_pctrans_prev={inline_query.from_user.id}-{target}'),
				InlineKeyboardButton(f'{a + 1}/{len(msgs)}', 'wikipedia_nop'),
				InlineKeyboardButton('Next', f'engine_pctrans_next={inline_query.from_user.id}-{target}')
			]
			if not a:
				buttons.pop(0)
			if len(msgs) == a + 1:
				buttons.pop()
			answers.append(InlineQueryResultArticle("Engine Privacy",
				InputTextMessageContent(result, disable_web_page_preview=True), reply_markup=InlineKeyboardMarkup([buttons]), id=f'pctrans{a}-{time.time()}', description="transaction")
			)
		await inline_query.answer(answers, is_personal=True)

@slave.on_callback_query(filters.regex('^engine_pctrans_(\w+)=(\d+)(?:-(\d+)|)$'))
@log_errors
async def translist_info(client, query):

	subcommand = query.matches[0].group(1)
	target = int(query.matches[0].group(3))

	if query.from_user.id not in app_user_ids:
		await query.answer('...no', cache_time=3600, show_alert=True)
		return

	async with message_lock:
		if target not in privacycom_queries:
			await query.answer('This message is too old', cache_time=3600, show_alert=True)
			return
		msgs, page = privacycom_queries[target]

	opage = page
	if subcommand == "next":
		page += 1
	if subcommand == "prev":
		page -= 1

	if page > len(msgs):
		page = len(msgs)
	if page < 0:
		page = 0

	if opage != page:
		buttons = [
				InlineKeyboardButton('Back', f'engine_pctrans_prev={query.from_user.id}-{target}'),
				InlineKeyboardButton(f'{page + 1}/{len(msgs)}', 'wikipedia_nop'),
				InlineKeyboardButton('Next', f'engine_pctrans_next={query.from_user.id}-{target}')
			]
		if not page:
			buttons.pop(0)
		if len(msgs) == page + 1:
			buttons.pop()
		await query.edit_message_text(msgs[page], disable_web_page_preview=True, reply_markup=InlineKeyboardMarkup([buttons]))
		async with message_lock:
			privacycom_queries[target] = msgs, page
	await query.answer()

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['translist', 'listtrans'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def translist(client, message: Message):
	"""{prefix}translist <i>[-d] &lt;card token&gt;</i> - Print transactions on a card (-d for debug session)"""
	# -translist -d <card token>
	command = message.command
	command.pop(0)
	if command:
		if command[0] == "-d":
			transactions = pcli_dbg.transactions_list(card_token=command[1])
			tokencrc = zlib.crc32(command[1].encode())
		else:
			transactions = pcli.transactions_list(card_token=command[0])
			tokencrc = zlib.crc32(command[0].encode())

		tz = pytz.timezone(config['config']['timezone'])
		trans = []
		for t in transactions:
			transinfo = f"\N{BANKNOTE WITH DOLLAR SIGN} Transaction\n"
			hashtag = "#transaction"
			if t.amount:
				amount = numbers.format_currency(t.amount / 100, "USD", locale='en')
				transinfo += f"<b>Amount</b>: {amount}\n"
			if t.settled_amount:
				settled_amount = numbers.format_currency(t.settled_amount / 100, "USD", locale='en')
				transinfo += f"<b>Settled</b>: {settled_amount}\n"
			if t.status:
				emoji = "\N{NO ENTRY}" if t.status == Status.DECLINED else "\N{WHITE HEAVY CHECK MARK}"
				transinfo += f"<b>Status</b>: {t.status.value} {emoji}\n"
				hashtag += f" #{t.status.value.lower()}"
			if t.result:
				transinfo += f"<b>Result</b>: {t.result.value}\n"
				hashtag += f" #{t.result.value.lower()}"
			if t.merchant.descriptor:
				transinfo += f"<b>Merchant</b>: {t.merchant.descriptor}\n"
			if t.created:
				date = t.created.astimezone(tz).strftime("%c")
				transinfo += f"<b>Date</b>: {date}\n"
			if t.token:
				transinfo += f"<b>Token</b>: <code>{t.token}</code>\n"
			transinfo += hashtag + "\n"
			trans.append(transinfo)

		privacycom_queries[tokencrc] = trans, 0
		x = await client.get_inline_bot_results((await slave.get_me()).username, f"engine_pctrans-{tokencrc}")
		await client.send_inline_bot_result(message.chat.id, query_id=x.query_id, result_id=x.results[0].id, hide_via=True)
	else:
		await self_destruct(message, "<code>invalid syntax</code>")



@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['newcard', 'issuecard'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def newcard(client, message: Message):
	"""{prefix}newcard <i>&lt;card type&gt; &lt;limit (in pennies)&gt; &lt;duration&gt; &lt;state&gt; &lt;memo&gt;</i> - Create a new card
Note: This is a debug-only command
	"""
	# -newcard <card type> <limit (in pennies)> <duration> <state> <memo>
	command = message.command
	command.pop(0)
	if len(command) >= 5:
		ctype = command[0]
		limit = command[1]
		duration = command[2]
		state = command[3]
		memo = command[4:]
		try:
			card = pcli_dbg.create_card(ctype, memo, limit, duration, state)
		except ValueError as e:
			await self_destruct(message, f"Error: {e.text}")
			return

		cinfo = f"<b>Card</b> <code>{card.token}</code>\n"
		if card.memo:
			cinfo += f"<b>Memo:</b> {card.memo}\n"
		if card.hostname:
			cinfo += f"<b>Hostname:</b> {card.hostname}\n"
		if card.pan:
			cinfo += f"<b>PAN:</b> {card.pan}\n"
		elif card.last_four:
			cinfo += f"<b>PAN:</b> **{card.last_four}\n"
		if card.exp_month and card.exp_year:
			cinfo += f"<b>EXP:</b> {card.exp_month}/{card.exp_year}"
			if card.cvv:
				cinfo += f" <b>CVV:</b> {card.cvv}"
			cinfo += "\n"
		if card.spend_limit:
			limit = int(card.spend_limit)/100
			cinfo += f"<b>Limit:</b> ${limit:,.2f} ({card.spend_limit_duration.value})\n"
		if card.state:
			cinfo += f"<b>State:</b> {card.state.value}\n"
		if card.type:
			cinfo += f"<b>Type:</b> {card.type.value}\n"
		await message.reply(cinfo)
		return
	await self_destruct(message, "<code>Invalid Syntax: newcard [card type] [limit (in pennies)] [duration] [state] [memo] </code>")


@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['updatecard', 'editcard'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def updatecard(client, message: Message):
	"""{prefix}updatecard <i>&lt;card token&gt; -s=&lt;state&gt; -l=&lt;spend limit&gt; -d=&lt;limit duration&gt; -m="&lt;memo&gt;"</i> - Update a card
Note: This is a debug-only command
	"""
	# -updatecard <card token> -s=<state> -l=<spend limit> -d=<limit duration> -m="<memo>"
	command = message.command
	command.pop(0)
	if command:
		# Token is always 1st argument
		token = command[0]
		# Start to parse command arguments.
		state = None
		limit = None
		duration = None
		memo = None
		found = False
		for c in command:
			if c.startswith("-s="):
				state = c.split('=')[1]
			if c.startswith("-l="):
				limit = c.split('=')[1]
			if c.startswith("-d="):
				duration = c.split('=')[1]
			if c.startswith("-m="):
				memo = c.split('=')[1]
			if memo and not found:
				if '"' in c:
					found = True
					c = c.strip('"')
				memo += c + " "
		if memo:
			memo = memo.strip()

		try:
			card = pcli_dbg.update_card(token, state, memo, limit, duration)
		except ValueError as e:
			await self_destruct(message, f"<code>Error: {e.text}</code>")
			return

		cinfo = f"<b>Card</b> <code>{card.token}</code>\n"
		if card.memo:
			cinfo += f"<b>Memo:</b> {card.memo}\n"
		if card.hostname:
			cinfo += f"<b>Hostname:</b> {card.hostname}\n"
		if card.pan:
			cinfo += f"<b>PAN:</b> {card.pan}\n"
		elif card.last_four:
			cinfo += f"<b>PAN:</b> **{card.last_four}\n"
		if card.exp_month and card.exp_year:
			cinfo += f"<b>EXP:</b> {card.exp_month}/{card.exp_year}"
			if card.cvv:
				cinfo += f" <b>CVV:</b> {card.cvv}"
			cinfo += "\n"
		if card.spend_limit:
			limit = int(card.spend_limit)/100
			cinfo += f"<b>Limit:</b> ${limit:,.2f} ({card.spend_limit_duration.value})\n"
		if card.state:
			cinfo += f"<b>State:</b> {card.state.value}\n"
		if card.type:
			cinfo += f"<b>Type:</b> {card.type.value}\n"
		await message.reply(cinfo)
	else:
		await self_destruct(message, "<code>Invalid Syntax: updatecard -s=[state] -l=[spend limit] -d=[limit duration] -m=\"[memo]\"</code>")

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['cardannounce'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def cardannounce(client, message: Message):
	"""{prefix}cardannounce <i>&lt;card token&gt; &lt;group id&gt; </i> - Announce incoming transactions in a group or DM"""
	# -cardannounce <card token> <channel id>
	command = message.command
	command.pop(0)
	chat = message.chat.id
	token = None
	if command:
		token = command[0]
		if len(command) > 1:
			chat = command[1]

	if not token:
		await self_destruct(message, "<code>You must specify a card token</code>")
		return

	try:
		chat, entity_client = await get_entity(client, chat)
	except:
		await self_destruct(message, "<code>Invalid chat or group</code>")
		return

	lel = await PrivacyComAnnounce.objects.get_or_none(channel_id=chat.id, token=token)
	if lel:
		await lel.delete()
		await message.edit(f"<code>No longer announcing transactions for card \"{token}\" in {chat.title}</code>")
	else:
		await PrivacyComAnnounce(channel_id=chat.id, token=token).save()
		await message.edit(f"<code>Announcing all transactions for card \"{token}\" in {chat.title}</code>")
	await asyncio.sleep(3)
	await message.delete()

#=================================================
# Simulation APIs
#=================================================
@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['simauth'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def simulate_cardauth(client, message: Message):
	"""{prefix}simauth <i>&lt;pan&gt; &lt;amount&gt; &lt;merchant descriptor&gt;</i> - Simulate a card preauthorization
Note: This is a debug-only command
	"""
	# -simauth <pan> <amount> <merchant descriptor>
	if not pcli_dbg:
		await self_destruct(message, "<code>You must set a debug token to use this command</code>")
		return

	command = message.command
	command.pop(0)
	if len(command) >= 3:
		pan = command[0]
		amount = int(command[1])
		desc = " ".join(command[2:])
		token = pcli_dbg.auth_simulate(desc, pan, amount)
		await message.reply("<code>" + html.escape(str(token)) + "</code>")
	else:
		await self_destruct(message, "<code>invalid syntax: simauth [pan] [amount] [merchant descriptor]</code>")


@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['simclear'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def simulate_cardclear(client, message: Message):
	"""{prefix}simclear <i>&lt;trans token&gt; &lt;amount&gt;</i> - Simulate a transaction settlement
Note: This is a debug-only command
	"""
	# -simclear <trans token> <amount>
	if not pcli_dbg:
		await self_destruct(message, "<code>You must set a debug token to use this command</code>")
		return

	command = message.command
	command.pop(0)
	if len(command) == 2:
		token = command[0]
		amount = int(command[1])
		resp = pcli_dbg.clearing_simulate(token, amount)
		await message.reply("<code>"+ html.escape(str(resp)) + "</code>")
	else:
		await self_destruct(message, "<code>invalid syntax: simclear [trans token] [amount]</code>")

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['simvoid'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def simulate_cardvoid(client, message: Message):
	"""{prefix}simvoid <i>&lt;trans token&gt; &lt;amount&gt;</i> - Simulate voiding a transaction
Note: This is a debug-only command
	"""
	# -simvoid <trans token> <amount>
	if not pcli_dbg:
		await self_destruct(message, "<code>You must set a debug token to use this command</code>")
		return

	command = message.command
	command.pop(0)
	if len(command) == 2:
		token = command[0]
		amount = int(command[1])
		resp = pcli_dbg.void_simulate(token, amount)
		await message.reply("<code>"+ html.escape(str(resp)) + "</code>")
	else:
		await self_destruct(message, "<code>invalid syntax: simvoid [trans token] [amount]</code>")

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['simreturn'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def simulate_cardreturn(client, message: Message):
	"""{prefix}simreturn <i>&lt;pan&gt; &lt;amount&gt; &lt;merchant descriptor&gt;</i> - Simulate a refund transaction
Note: This is a debug-only command
	"""
	# -simreturn <pan> <amount> <merchant descriptor>
	if not pcli_dbg:
		await self_destruct(message, "<code>You must set a debug token to use this command</code>")
		return

	command = message.command
	command.pop(0)
	if len(command) >= 3:
		pan = command[0]
		amount = int(command[1])
		desc = " ".join(command[2:])
		token = pcli_dbg.return_simulate(desc, pan, amount)
		await message.reply("<code>" + html.escape(str(token)) + "</code>")
	else:
		await self_destruct(message, "<code>invalid syntax: simreturn [pan] [amount] [merchant descriptor]</code>")

__signature__ = "SHSIG-IE70p+YFsnwCf2s8E4spYdCyh6d7TEwxYnMO4ECX7b/IAAAAIKZwyHIjWv/2E/tvtRtNeYQDtJWT/xMvzwVGAop7+hQuAAAA"