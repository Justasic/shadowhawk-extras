import ziproto, asyncio, time, ormar
from pyrogram import Client, filters, ContinuePropagation
from pyrogram.types import Message, Chat, User
from pyrogram.raw.types import InputPeerChat, InputPeerChannel
from asyncevents import on_event
from shadowhawk import config
from shadowhawk.utils import get_entity, self_destruct
from shadowhawk.utils.Logging import log_errors, public_log_errors
from shadowhawk.database import BaseModel
from collections import deque

# All the allowed sudoers, loaded into ram
# This could be bad but oh well we shouldn't have too many
SUDOERS = {}
# Handle the already handled messages from other clients
handled = deque(maxlen=200)

# This is added so more operators can query information
# on spam-adders and build reports. 
class UserbotSudoers(ormar.Model):
	class Meta(BaseModel):
		tablename = 'Superusers'
	
	id: int =  ormar.Integer(primary_key=True)
	blob: bytes = ormar.LargeBinary(max_length=1073742000)

@on_event("OnDatabaseStart")
async def OnDBStart(_, EventName: str):
	global SUDOERS
	# Populate our global
	# sql_users = (await session.execute(select(UserbotSudoers))).fetchall()
	sql_users = await UserbotSudoers.objects.all()
	for user in sql_users:
		SUDOERS[user.id] = ziproto.decode(user.blob)

async def is_sudo(chat: Chat, user: User):
	global SUDOERS
	# Check if the user in the chat is
	# allowed to send commands here.
	chat_id = int(str(chat.id)[4:])
	# If they're not a sudo, bye bye
	if user.id not in SUDOERS:
		return False

	# If they're a sudo but not in the right chat
	# if chat_id not in SUDOERS[user.id][0x01]:
	# 	return False
	# Grant sudo command.
	return True

# Create a new filter
async def sudoers_filter(_, __, m: Message):
	global SUDOERS
	if m.from_user:
		# Check if they're sudo or not.
		d = await is_sudo(m.chat, m.from_user)
		if d:
			# We've handled this specific message, move on.
			if m.id in handled:
				return False
			# Append to the handled messages.
			handled.append(m.id)
		return d
	else:
		return False

# Create the filter
sudo = filters.create(sudoers_filter)

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['lssudo', 'listsudo', 'listsudos'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def ls_sudo(client: Client, message: Message):

	sudos = await UserbotSudoers.objects.all()
	text = "<b>List of Super Users</b>\n"
	text_unping = text
	for s in sudos:
		su, cl = await get_entity(client, s.id)
		text += f"- <a href=\"tg://user?id={s.id}\">{su.first_name} {su.last_name}</a> <code>[{s.id}]</code>\n"
		text_unping += f"- {su.first_name} {su.last_name} <code>[{s.id}]</code>\n"
	
	msg = await message.reply(text_unping, disable_web_page_preview=True)
	msg = await msg.edit(text, disable_web_page_preview=True)
	

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['addsudo'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def add_sudo(client: Client, message: Message):
	# -addsudo <username> <chat_id> [<chat_id>, ...]
	command = message.command
	command.pop(0)

	if not command:
		await self_destruct(message, "<code>are you okay?</code>")
		raise ContinuePropagation

	user, u_client = await get_entity(client, command[0])

	# First, query the DB, we may be adding more chats to this user.
	sql_user = await UserbotSudoers.objects.get_or_none(id=user.id)

	# Iterate the other inputs
	chats = []
	for arg in command[1:]:
		ent, e_cl = await get_entity(client, arg)
		peer = await e_cl.resolve_peer(ent.id)
		# ignore nonsense
		if isinstance(peer, InputPeerChat):
			chats.append(peer.chat_id)
		if isinstance(peer, InputPeerChannel):
			chats.append(peer.channel_id)
		# Sleep to prevent floodwaits if we're adding a lot of chats
		await asyncio.sleep(1)
	
	if not chats:
		await self_destruct(message, f"<code>No chats to add {user.first_name} as super user</code>")
		raise ContinuePropagation

	# used later
	zipro = {}
	if not sql_user:
		# build the ziproto structure
		zipro = {
			0x00: user.id,   # User's ID
			0x01: chats,     # Peer ID of the chats they're allowed in 
			0x02: time.time()# Time they were made sudo
		}
		sql_user = UserbotSudoers(user.id, ziproto.encode(zipro))
	else:
		zipro = ziproto.decode(sql_user.blob)
		zipro[0x01] = list(dict.fromkeys(zipro[0x01] + chats))
		sql_user.blob = ziproto.encode(zipro)

	# Update our global
	SUDOERS[user.id] = zipro

	await sql_user.save()
	await self_destruct(message, f"<code>Added {user.first_name} to super users</code>")

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.forwarded & filters.me & filters.command(['rmsudo', 'delsudo'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def rm_sudo(client: Client, message: Message):
	# -rmsudo <username> [<chat_id>, ...]
	command = message.command
	command.pop(0)

	if not command:
		await self_destruct(message, "<code>are you okay?</code>")
		raise ContinuePropagation

	user, u_client = await get_entity(client, command[0])

	# First, query the DB, we may be adding more chats to this user.
	sql_user = await UserbotSudoers.objects.get_or_none(id=user.id)

	# Iterate the other inputs
	chats = []
	if command[1:]:
		for arg in command[1:]:
			ent, e_cl = await get_entity(client, arg)
			peer = await e_cl.resolve_peer(ent.id)
			# ignore nonsense
			if isinstance(peer, InputPeerChat):
				chats.append(peer.chat_id)
			if isinstance(peer, InputPeerChannel):
				chats.append(peer.channel_id)
			# Sleep to prevent floodwaits if we're adding a lot of chats
			await asyncio.sleep(1)

		# decode blob
		zipro = ziproto.decode(sql_user.blob)
		# OK now we get a difference in chats.
		diff = [c for c in zipro[0x01] if c not in chats]
		# delete the object
		if not diff:
			await sql_user.delete()
			del SUDOERS[user.id]
			await self_destruct(message, f"<code>{user.first_name} is no longer a super user</code>")
			raise ContinuePropagation
		# update our chats
		zipro[0x01] = diff
		# encode blob
		sql_user.blob = ziproto.encode(zipro)

		# Update our global
		SUDOERS[user.id] = zipro
	else:
		await sql_user.delete()
		del SUDOERS[user.id]
		await self_destruct(message, f"<code>{user.first_name} is no longer a super user</code>")

__signature__ = "SHSIG-IBpkfospDn+oyVCIeyS8W3iR42ZgRDh79hWBWP1QktoXAAAAINBK4WzolqW+lc/2l/Yca85LCMNA2iS0UW6/0HTQIgaUAAAA"